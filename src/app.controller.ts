import { Controller, Get, Inject, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { AccountService } from './interfaces/services/account-service.interface';

@Controller()
export class AppController {
  constructor(
    @Inject('AccountService') private readonly accountService: AccountService
  ) {}

  @Get('/api/accounts/:customerId/customer')
  async findAccountByCustomerId(@Param('customerId') customerId: number) {
    return await this.accountService.findAccountByCustomerId(customerId);
  }
}

import { ConfigService, ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BankSharedModule } from 'bank-shared-lib';
import { AppController } from './app.controller';
import { Module, Global, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppService } from './app.service';
import { Account } from './entities/account.entity';
import { AccountRepository } from './repositories/account.repository';
import { SequenceAccountNoGenerator } from './implementations/sequence-account-no-generator.impl';
import { BrokerConfig, DatabaseConfig, ServiceConfig, SystemConfig, AuthConfig } from './configs/configuration';
import { GenerateAccountNoHandler } from './commands/handlers/generate-account-no.handler';
import { RuleContext } from './implementations/rule-context.impl';
import { GenerateAccountHandler } from './commands/handlers/generate-account.handler';
import { AccountServiceImpl } from './implementations/services/account-service.impl';
import { PositiveBalanceRule, MinimumBalanceRule } from './implementations/validations/balance-rule.impl';
import { BalanceServiceImpl } from './implementations/services/balance-service.impl';
import { BlockedStatusRule, UnblockStatusRule, ClosedStatusRule } from './implementations/validations/status-rule.service';
import { SubstractRequesterBalanceHandler } from './commands/handlers/transfers/substract-requester-balance.handler';
import { AddDestinationBalanceHandler } from './commands/handlers/transfers/add-destination-balance.handler';
import { RollbackRequesterBalanceHandler } from './commands/handlers/transfers/rollback-requester-balance.handler';
import { WithdrawFromOriginAccountHandler } from './commands/handlers/withdrawals/withdraw-from-origin-account.handler';
import { WithdrawFromBankAccountHandler } from './commands/handlers/withdrawals/withdraw-from-bank-account.handler';
import { RollbackOriginWithdrawalHandler } from './commands/handlers/withdrawals/rollback-origin-withdrawal.handler';
import { DepositToOriginAccountHandler } from './commands/handlers/deposits/deposit-to-origin-account.handler';
import { DepositToBankAccountHandler } from './commands/handlers/deposits/deposit-to-bank-account.handler';
import { RollbackOriginDepositHandler } from './commands/handlers/deposits/rollback-origin-deposit.handler';
import { BankCustomerCreateAccountNoHandler } from './commands/handlers/customers/bank-customer-create-account-no.handler';
import { BankCustomerCreateAccountHandler } from './commands/handlers/customers/bank-customer-create-account.handler';
import { CustomerQuery } from './queries/customer.query';
import { CustomerAccountServiceImpl } from './implementations/services/customer-account-service.impl';
import { UserAccountController } from './controllers/user-account.controller';
import { UserAuthMiddleware } from './middlewares/user-auth.middleware';
import { BankAccountController } from './controllers/bank-account.controller';
import { BankAuthMiddleware } from './middlewares/bank-auth.middleware';
import { BlacklistCustomerAccountHandler } from './commands/handlers/customers/blacklist-customer-account.handler';
import { UnblacklistCustomerAccountHandler } from './commands/handlers/customers/unblacklist-customer-account.handler';

const sharedServiceFactory = [
  {
    provide: 'AccountService',
    useFactory: (
      accountRepository, 
      configService, 
      accountNoGenerator, 
      newAccountRule,
      blockRule,
      unblockRule,
      closeRule
    ) => {
      return new AccountServiceImpl(
        accountRepository, 
        configService, 
        accountNoGenerator, 
        newAccountRule, 
        blockRule, 
        unblockRule, 
        closeRule
      );
    },
    inject: [
      AccountRepository, 
      ConfigService, 
      'AccountNoGenerator', 
      'NewAccountRule',
      'BlockRule',
      'UnblockRule',
      'CloseRule'
    ]
  },
  {
    provide: 'BalanceService',
    useFactory: (repository, accountService, balanceRule, configService) => {
      return new BalanceServiceImpl(repository, accountService, balanceRule, configService);
    },
    inject: [AccountRepository, 'AccountService', 'BalanceRule', ConfigService]
  },
  {
    provide: 'CustomerAccountService',
    useClass: CustomerAccountServiceImpl
  },
]

@Global()
@Module({
  imports: [
    BankSharedModule.registerHandler({
      useFactory: (brokerPublisher, accountService, accountNoGenerator, balanceService, customerAccountService) => ({
        commandList: [
          new GenerateAccountNoHandler(brokerPublisher, accountNoGenerator),
          new GenerateAccountHandler(brokerPublisher, accountService),
          new SubstractRequesterBalanceHandler(brokerPublisher, balanceService),
          new AddDestinationBalanceHandler(brokerPublisher, balanceService),
          new RollbackRequesterBalanceHandler(brokerPublisher, balanceService),
          new WithdrawFromOriginAccountHandler(brokerPublisher, balanceService),
          new WithdrawFromBankAccountHandler(brokerPublisher, balanceService),
          new RollbackOriginWithdrawalHandler(brokerPublisher, balanceService),
          new DepositToOriginAccountHandler(brokerPublisher, balanceService),
          new DepositToBankAccountHandler(brokerPublisher, balanceService),
          new RollbackOriginDepositHandler(brokerPublisher, balanceService),
          new BankCustomerCreateAccountNoHandler(brokerPublisher, accountNoGenerator),
          new BankCustomerCreateAccountHandler(brokerPublisher, accountService),
          new BlacklistCustomerAccountHandler(brokerPublisher, customerAccountService),
          new UnblacklistCustomerAccountHandler(brokerPublisher, customerAccountService)
        ]
      }),
      inject: ['BrokerPublisher', 'AccountService', 'AccountNoGenerator', 'BalanceService', 'CustomerAccountService']
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get<string>('database.host'),
          port: configService.get<number>('database.port'),
          username: configService.get<string>('database.username'),
          password: configService.get<string>('database.password'),
          database: configService.get<string>('database.name'),
          entities: [Account],
          synchronize: true
        }
      },
      inject: [ConfigService]
    }),
    TypeOrmModule.forFeature([AccountRepository]),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [BrokerConfig, DatabaseConfig, ServiceConfig, SystemConfig, AuthConfig]
    })
  ],
  controllers: [AppController, UserAccountController, BankAccountController],
  providers: [
    AppService,
    {
      provide: 'AccountNoGenerator',
      useFactory: (accountRepository, configService) => {
        return new SequenceAccountNoGenerator(accountRepository, configService);
      },
      inject: [AccountRepository, ConfigService]
    },
    {
      provide: 'NewAccountRule',
      useFactory: (configService) => {
        return new RuleContext(
          [new PositiveBalanceRule(), new MinimumBalanceRule(configService)]
        );
      },
      inject: [ConfigService]
    },
    {
      provide: 'BalanceRule',
      useFactory: (configService) => {
        return new RuleContext([new PositiveBalanceRule(), new MinimumBalanceRule(configService)]);
      },
      inject: [ConfigService]
    },
    {
      provide: 'BlockRule',
      useFactory: () => {
        return new RuleContext([new BlockedStatusRule()]);
      },
    },
    {
      provide: 'UnblockRule',
      useFactory: () => {
        return new RuleContext([new UnblockStatusRule()]);
      }
    },
    {
      provide: 'CloseRule',
      useFactory: () => {
        return new RuleContext([new ClosedStatusRule()]);
      }
    },
    {
      provide: 'CustomerQuery',
      useClass: CustomerQuery
    },
    ...sharedServiceFactory
  ],
  exports: [
    {
      provide: 'AccountNoGenerator',
      useFactory: (accountRepository, configService) => {
        return new SequenceAccountNoGenerator(accountRepository, configService);
      },
      inject: [AccountRepository, ConfigService]
    },
    ...sharedServiceFactory
  ]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(UserAuthMiddleware)
      .forRoutes(UserAccountController)

    consumer
      .apply(BankAuthMiddleware)
      .forRoutes(BankAccountController)
  }
}

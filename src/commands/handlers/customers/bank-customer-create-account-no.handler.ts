import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { AccountNoGenerator } from './../../../interfaces/account-no-generator.interface';

export class BankCustomerCreateAccountNoHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly accountNoGenerator: AccountNoGenerator,
    ) {
        super();
    }
    getCommandName(): string {
        return 'bankCustomerCreateAccountNoCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, branchCode } = data;

        try {
            const accountNo = await this.accountNoGenerator.generateNewNumber(branchCode);
            await this.brokerPublisher.dispatch('bankCustomerAccountNoCreatedEvent', { stateId: stateId, accountNo: accountNo });
        } catch(error) {
            await this.brokerPublisher.dispatch('bankCustomerCreateAccountNoFailedEvent', { stateId: stateId, error: error.message });
        }
    }
}
import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { AccountService } from './../../../interfaces/services/account-service.interface';

export class BankCustomerCreateAccountHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly accountService: AccountService
    ) {
        super();
    }
    getCommandName(): string {
        return 'bankCustomerCreateAccountCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, accountNo, branchCode, customerId, account } = data;
        account.branchCode = branchCode;
        account.customerId = customerId;

        try {
            const accountData = await this.accountService.createNewAccount(account, accountNo);
            await this.brokerPublisher.dispatch('bankCustomerAccountCreatedEvent', { stateId: stateId, accountId: accountData.id });
        } catch(error) {
            await this.brokerPublisher.dispatch('bankCustomerCreateAccountFailedEvent', { stateId: stateId, error: error.message });
        }
    }
}
import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CustomerAccountService } from './../../../interfaces/services/customer-account-service.interface';

export class BlacklistCustomerAccountHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly customerAccountService: CustomerAccountService
    ) {
        super();
    }
    getCommandName(): string {
        return 'blacklistCustomerAccountCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, id } = data;

        try {
            await this.customerAccountService.blockCustomerAccounts(id);
            await this.brokerPublisher.dispatch('customerAccountBlacklistedEvent', { stateId: stateId });
        } catch(error) {
            await this.brokerPublisher.dispatch('blacklistCustomerAccountFailedEvent', { stateId: stateId, error: error.message });
        }
    }
}
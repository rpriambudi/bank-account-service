import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CustomerAccountService } from './../../../interfaces/services/customer-account-service.interface';

export class UnblacklistCustomerAccountHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly customerAccountService: CustomerAccountService
    ) {
        super();
    }
    getCommandName(): string {
        return 'unblacklistCustomerAccountCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { stateId, id } = data;

        try {
            await this.customerAccountService.unblockCustomerAccounts(id);
            await this.brokerPublisher.dispatch('customerAccountUnblacklistedEvent', { stateId: stateId });
        } catch(error) {
            await this.brokerPublisher.dispatch('unblacklistCustomerAccountFailedEvent', { stateId: stateId, error: error.message });
        }
    }
}
import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class DepositToBankAccountHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }

    getCommandName(): string {
        return 'depositToBankAccountCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { branchCode, amount, stateId, transactionId } = data;

        try {
            await this.balanceService.addBranchAccountBalance(branchCode, amount);
            await this.brokerPublisher.dispatch('bankAccountDepositedEvent', { stateId: stateId, transactionId: transactionId });
        } catch(error) {
            await this.brokerPublisher.dispatch('depositToBankFailedEvent', { stateId: stateId, error: error.message });
        }
    }
    
}
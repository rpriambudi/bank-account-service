import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class RollbackOriginDepositHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }

    getCommandName(): string {
        return 'rollbackOriginDepositCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { accountNo, amount, stateId, transactionId } = data;
        await this.balanceService.substractAccountBalance(accountNo, amount);
        await this.brokerPublisher.dispatch('originDepositRollbackedEvent', { stateId: stateId, transactionId: transactionId });
    }
    
}
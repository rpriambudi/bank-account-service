import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { AccountNoGenerator } from './../../interfaces/account-no-generator.interface';

export class GenerateAccountNoHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly accountNoGenerator: AccountNoGenerator,
    ) {
        super();
    }
    getCommandName(): string {
        return 'generateAccountNoCommand'
    }    
    
    async handle(data: any): Promise<any> {
        try {
            const accountNo = await this.accountNoGenerator.generateNewNumber(data.branchCode);
            const eventData = { stateId: data.stateId, accountNo: accountNo};
            await this.brokerPublisher.dispatch('accountNoGeneratedEvent', eventData);
        } catch (error) {
            await this.brokerPublisher.dispatch('generateAccountNoFailedEvent', { stateId: data.stateId, error: error.message });
        }
    }
}
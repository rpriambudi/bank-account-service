import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { CreateAccountDto } from './../../dto/create-account.dto';
import { AccountService } from './../../interfaces/services/account-service.interface';

export class GenerateAccountHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly accountService: AccountService
    ) {
        super();
    }
    getCommandName(): string {
        return 'generateAccountCommand'
    }    
    
    async handle(data: any): Promise<any> {
        const createAccountDto: CreateAccountDto = {
            balance: 0,
            branchCode: data.branchCode,
            customerId: data.customerId,
        }

        try {
            const account = await this.accountService.createNewEmptyAccount(createAccountDto, data.accountNo);
            await this.brokerPublisher.dispatch('accountGeneratedEvent', { stateId: data.stateId, accountId: account.id });
        } catch(error) {
            await this.brokerPublisher.dispatch('generateAccountFailedEvent', { stateId: data.stateId, error: error.message });
        }
    }
}
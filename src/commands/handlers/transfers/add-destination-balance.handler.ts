import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class AddDestinationBalanceHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }

    getCommandName(): string {
        return 'addDestinationBalanceCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { destinationAcctNo, amount, stateId, transactionId } = data;

        try {
            await this.balanceService.addAccountBalance(destinationAcctNo, amount);
            await this.brokerPublisher.dispatch('destinationBalanceAddedEvent', { stateId: stateId, transactionId: transactionId });
        } catch(error) {
            await this.brokerPublisher.dispatch('addDestinationBalanceFailedEvent', { stateId: stateId, error: error.message });
        }
    }
}
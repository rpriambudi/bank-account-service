import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class RollbackRequesterBalanceHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }

    getCommandName(): string {
        return 'rollbackRequesterBalanceCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { requesterAcctNo, amount, stateId, errorMessage } = data;

        await this.balanceService.addAccountBalance(requesterAcctNo, amount);
        await this.brokerPublisher.dispatch(
            'requesterBalanceRollbackedEvent', 
            { stateId: stateId, error: errorMessage }
        );
    }
    
}
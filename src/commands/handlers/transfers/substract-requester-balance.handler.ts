import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class SubstractRequesterBalanceHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }
    getCommandName(): string {
        return 'substractRequesterBalanceCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { requesterAcctNo, amount, stateId, transactionId } = data;

        try {
            await this.balanceService.substractAccountBalance(requesterAcctNo, amount);
            await this.brokerPublisher.dispatch('requesterBalanceSubstractedEvent', { stateId: stateId, transactionId: transactionId });
        } catch(error) {
            await this.brokerPublisher.dispatch('substractRequesterBalanceFailedEvent', { stateId: stateId, error: error.message });
        }
    }

}
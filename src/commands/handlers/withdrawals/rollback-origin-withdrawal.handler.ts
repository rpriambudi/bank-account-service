import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class RollbackOriginWithdrawalHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }

    getCommandName(): string {
        return 'rollbackOriginWithdrawalCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { accountNo, amount, stateId, transactionId } = data;
        await this.balanceService.addAccountBalance(accountNo, amount);
        await this.brokerPublisher.dispatch('originWithdrawalRollbackedEvent', { stateId: stateId, transactionId: transactionId });
    }
    
}
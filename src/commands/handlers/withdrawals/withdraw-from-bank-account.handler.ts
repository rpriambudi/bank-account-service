import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class WithdrawFromBankAccountHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }

    getCommandName(): string {
        return 'withdrawFromBankAccountCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { branchCode, amount, stateId, transactionId } = data;

        try {
            await this.balanceService.substractBranchAccountBalance(branchCode, amount);
            await this.brokerPublisher.dispatch('bankAccountWithdrawnEvent', { stateId: stateId, transactionId: transactionId });
        } catch(error) {
            await this.brokerPublisher.dispatch('withdrawFromBankFailedEvent', { stateId: stateId, error: error.message });
        }
    }
    
}
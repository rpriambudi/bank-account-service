import { BaseConsumer, IBrokerPublisher } from 'bank-shared-lib';
import { BalanceService } from './../../../interfaces/services/balance-service.interface';

export class WithdrawFromOriginAccountHandler extends BaseConsumer {
    constructor(
        private readonly brokerPublisher: IBrokerPublisher,
        private readonly balanceService: BalanceService
    ) {
        super();
    }

    getCommandName(): string {
        return 'withdrawFromOriginAccountCommand';
    }    
    
    async handle(data: any): Promise<any> {
        const { accountNo, amount, stateId, transactionId } = data;

        try {
            await this.balanceService.substractAccountBalance(accountNo, amount);
            await this.brokerPublisher.dispatch('originAccountWithdrawnEvent', { stateId: stateId, transactionId: transactionId });
        } catch(error) {
            await this.brokerPublisher.dispatch('withdrawFromOriginFailedEvent', { stateId: stateId, error: error.message });
        }
    }
    
}
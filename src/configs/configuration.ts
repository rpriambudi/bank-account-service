export const DatabaseConfig = () => ({
    database: {
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        name: process.env.DB_NAME
    }
})

export const AuthConfig = () => ({
    auth: {
        jwksUrl: process.env.KEYCLOAK_JWKS_URL,
        clientId: process.env.KEYCLOAK_CLIENT_ID,
    }
})

export const BrokerConfig = () => ({
    broker: {
        host: process.env.BROKER_HOST
    }
})

export const ServiceConfig = () => ({
    branch: {
        serviceUrl: process.env.BRANCH_SERVICE_URL
    },
    customer: {
        serviceUrl: process.env.CUSTOMER_SERVICE_URL
    }
})

export const SystemConfig = () => ({
    account: {
        sequenceName: process.env.ACCOUNT_SEQUENCE_NAME
    }
})
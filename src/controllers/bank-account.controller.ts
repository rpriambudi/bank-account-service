import { Controller, Post, Param, Body, Inject } from "@nestjs/common";
import { AuthContext, UserContext } from "bank-shared-lib";
import { CreateAccountDto } from "./../dto/create-account.dto";
import { AccountService } from "./../interfaces/services/account-service.interface";
import { CustomerQuery } from "./../queries/customer.query";
import { CustomerAccountService } from "./../interfaces/services/customer-account-service.interface";

@Controller()
export class BankAccountController {
    constructor(
        @Inject('AccountService') private readonly accountService: AccountService,
        @Inject('CustomerAccountService') private readonly customerAccountService: CustomerAccountService,
        @Inject('CustomerQuery') private readonly customerQuery: CustomerQuery
    ) {}
    
    @Post('/api/accounts/create/:cifNo')
    async createNewAccount(
        @Param('cifNo') cifNo: string,
        @Body() createDto: CreateAccountDto,
        @AuthContext() userContext: UserContext
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(cifNo);

        createDto.branchCode = userContext.branchCode;
        createDto.customerId = customer.id;
        return await this.accountService.createNewAccount(createDto);
    }

    @Post('/api/accounts/block/:accountNo')
    async blockAccount(
        @Param('accountNo') accountNo: string
    ) {
        return await this.accountService.blockAccount(accountNo);
    }

    @Post('/api/accounts/unblock/:accountNo')
    async unblockAccount(
        @Param('accountNo') accountNo: string
    ) {
        return await this.accountService.unblockAccount(accountNo);
    }

    @Post('/api/accounts/close/:accountNo')
    async closeAccount(
        @Param('accountNo') accountNo: string
    ) {
        return await this.accountService.closeAccount(accountNo);
    }

    @Post('/api/accounts/customer/block/:cifNo/cif')
    async blockCustomerAccounts(
        @Param('cifNo') cifNo: string
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(cifNo);
        return await this.customerAccountService.blockCustomerAccounts(customer.id);
    }

    @Post('/api/accounts/customer/unblock/:cifNo/cif')
    async unblockCustomerAccounts(
        @Param('cifNo') cifNo: string
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(cifNo);
        return await this.customerAccountService.unblockCustomerAccounts(customer.id);
    }

    @Post('/api/accounts/customer/close/:cifNo/cif')
    async closeCustomerAccounts(
        @Param('cifNo') cifNo: string
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(cifNo);
        return await this.customerAccountService.closeCustomerAccounts(customer.id);
    }
}
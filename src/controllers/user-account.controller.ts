import { Controller, Get, Inject, Post, Param } from "@nestjs/common";
import { AuthContext, UserContext } from "bank-shared-lib";
import { CustomerQuery } from "./../queries/customer.query";
import { BalanceService } from "./../interfaces/services/balance-service.interface";
import { CustomerAccountService } from "./../interfaces/services/customer-account-service.interface";

@Controller()
export class UserAccountController {
    constructor(
        @Inject('BalanceService') private readonly balanceService: BalanceService,
        @Inject('CustomerAccountService') private readonly customerAccountService: CustomerAccountService,
        @Inject('CustomerQuery') private readonly customerQuery: CustomerQuery
    ) {}

    @Get('/api/accounts/customer/balance')
    async getCustomerBalance(
        @AuthContext() userContext: UserContext
    ){
        const customer = await this.customerQuery.getCustomerByCifNo(userContext.cifNo);
        return await this.balanceService.checkBalanceByCustomerId(customer.id);
    }

    @Post('/api/accounts/customer/block')
    async blockCustomerAccounts(
        @AuthContext() userContext: UserContext
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(userContext.cifNo);
        return await this.customerAccountService.blockCustomerAccounts(customer.id)
    }

    @Post('/api/accounts/customer/block/:accountNo')
    async blockCustomerSelectedAccount(
        @Param('accountNo') accountNo: string,
        @AuthContext() userContext: UserContext
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(userContext.cifNo);
        return await this.customerAccountService.blockCustomerSelectedAccount(customer.id, accountNo);
    }

    @Post('/api/accounts/customer/unblock')
    async unblockCustomerAccounts(
        @AuthContext() userContext: UserContext
    ) {
        const customer = await this.customerQuery.getCustomerByCifNo(userContext.cifNo);
        return await this.customerAccountService.unblockCustomerAccounts(customer.id);
    }
}
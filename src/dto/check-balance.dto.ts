export class CheckBalanceDto {
    accountNo: string;
    balance: number;
}
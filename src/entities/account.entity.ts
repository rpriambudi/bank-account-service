import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

export enum StatusEnum {
    Live = 'LIVE',
    Closed = 'CLOSED',
    Blocked = 'BLOCKED'
}

@Entity()
export class Account {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        unique: false,
        name: 'branchId'
    })
    branchId: number

    @Column({
        type: 'int',
        unique: false,
        name: 'customer_id'
    })
    customerId: number

    @Column({
        type: 'varchar',
        unique: true,
        name: 'account_no'
    })
    accountNo: string

    @Column({
        type: 'bigint',
        unique: false,
        name: 'balance'
    })
    balance: number;

    @Column({
        type: 'varchar',
        unique: false,
        name: 'opened_at'
    })
    openedAt: string

    @Column({
        type: 'varchar',
        unique: false,
        name: 'status',
        default: StatusEnum.Live
    })
    status: string
}
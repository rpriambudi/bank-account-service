import { GenericException } from 'bank-shared-lib';

export class AccountNotFoundException extends GenericException {
    getDisplayCode(): string {
        return 'ACCOUNT_NOT_FOUND';
    }

    getErrorCode(): string {
        return '400302';
    }
    
    constructor(message: string) {
        super(message);
    }
}
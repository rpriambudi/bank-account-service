import { GenericException } from 'bank-shared-lib';

export class BlockedAccountException extends GenericException {
    getDisplayCode(): string {
        return 'BLOCKED_ACCOUNT';
    }

    getErrorCode(): string {
        return '400306';
    }
    

    constructor(message: string) {
        super(message);
    }
}
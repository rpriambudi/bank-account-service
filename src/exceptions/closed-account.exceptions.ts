import { GenericException } from 'bank-shared-lib';

export class ClosedAccountException extends GenericException {
    getDisplayCode(): string {
        return 'CLOSED_ACCOUNT';
    }

    getErrorCode(): string {
        return '400305';
    }
    
    constructor(message: string) {
        super(message);
    }
}
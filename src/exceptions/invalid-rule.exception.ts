import { GenericException } from 'bank-shared-lib';

export class InvalidRuleException extends GenericException {
    getDisplayCode(): string {
        return 'INVALID_RULE';
    }

    getErrorCode(): string {
        return '400307';
    }

    constructor(message: string) {
        super(message);
    }
}
import { GenericException } from 'bank-shared-lib';

export class NegativeBalanceException extends GenericException {
    getDisplayCode(): string {
        return 'NEGATIVE_BALANCE';
    }

    getErrorCode(): string {
        return '400301';
    }

    constructor(message: string) {
        super(message);
    }
}
import { InjectRepository } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { Account } from './../entities/account.entity';
import { AccountRepository } from './../repositories/account.repository';
import { AccountNoGenerator } from './../interfaces/account-no-generator.interface';

export class SequenceAccountNoGenerator implements AccountNoGenerator {
    constructor(
        @InjectRepository(Account) private readonly accountRepository: AccountRepository,
        private readonly configService: ConfigService
    ) {}

    async generateNewNumber(branchCode: string): Promise<string> {
        const sequenceName = this.configService.get('account.sequenceName');
        let latestSeq = await this.accountRepository.getSequence(sequenceName);
        if (latestSeq.length < 16) {
            const trailingZeroSize = 16 - latestSeq.length;
            let trailingZero = ''
            for (let i = 0; i < trailingZeroSize; i++) {
                trailingZero += '0'
            }
            latestSeq = trailingZero + latestSeq
        }
        return branchCode + '' + latestSeq;
    }
}
import axios from 'axios';
import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { AccountRepository } from '../../repositories/account.repository';
import { CreateAccountDto } from './../../dto/create-account.dto';
import { AccountServiceImpl } from './account-service.impl';
import { StatusEnum, Account } from './../../entities/account.entity';
import { NegativeBalanceException } from './../../exceptions/negative-balance.exception';
import { AccountNotFoundException } from './../../exceptions/account-not-found.exception';
import { RuleContext } from './../rule-context.impl';
import { MinimumBalanceException } from './../../exceptions/minimum-balance.exception';
import { SequenceAccountNoGenerator } from './../sequence-account-no-generator.impl';
jest.mock('./../rule-context.impl');
jest.mock('axios');

describe('AccountService', () => {
    let accountService: AccountServiceImpl;
    let repository: AccountRepository;
    let validationRule: RuleContext;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [ConfigModule],
            providers: [
                {
                    provide: 'AccountService',
                    useFactory: (accountRepository, validationRule, accountNoGenerator, configService) => {
                        return new AccountServiceImpl(accountRepository, configService, accountNoGenerator, validationRule, validationRule, validationRule, validationRule);
                    },
                    inject: [AccountRepository, 'ValidationRule', 'AccountNoGenerator', ConfigService]
                },
                {
                    provide: getRepositoryToken(Account),
                    useClass: AccountRepository
                },
                {
                    provide: 'ValidationRule',
                    useClass: RuleContext
                },
                {
                    provide: 'AccountNoGenerator',
                    useFactory: (accountRepository, configService) => {
                        return new SequenceAccountNoGenerator(accountRepository, configService);
                    },
                    inject: [AccountRepository, ConfigService]
                }
            ]
        }).compile();
        
        accountService = moduleRef.get<AccountServiceImpl>('AccountService');
        repository = moduleRef.get<AccountRepository>(getRepositoryToken(Account));
        validationRule = moduleRef.get<RuleContext>('ValidationRule');
    })

    describe('create method', () => {
        it('positive test. should return one Account object.', async () => {
            const createAccountDto: CreateAccountDto = {
                balance: 500000,
                branchCode: '001',
                customerId: 1
            }

            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            const branch = {
                id: 999,
                branchCode: '999',
                name: 'TEST_CLIENT',
                address: 'TEST_CLIENT'
            }

            jest.spyOn(validationRule, 'validate').mockImplementationOnce(async () => {
                return null;
            })
            jest.spyOn(axios, 'get').mockResolvedValueOnce({data: branch});
            jest.spyOn(repository, 'create').mockReturnValueOnce(testAccount);
            jest.spyOn(repository, 'save').mockResolvedValueOnce(testAccount);
            jest.spyOn(repository, 'getSequence').mockResolvedValueOnce('1');
            expect(await accountService.createNewAccount(createAccountDto)).toBe(testAccount);
        })

        it ('negative test, balance below than zero. should throw NegativeBalanceException', async () => {
            const createAccountDto: CreateAccountDto = {
                balance: -100000,
                branchCode: '001',
                customerId: 1
            }

            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: -1000000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            jest.spyOn(validationRule, 'validate').mockImplementationOnce(async () => {
                throw new NegativeBalanceException('Balance insufficient. Balance is negative.');
            })
            jest.spyOn(repository, 'create').mockReturnValueOnce(testAccount);

            try {
                expect(await accountService.createNewAccount(createAccountDto)).toBeCalled();
            } catch (error) {
                expect(error instanceof NegativeBalanceException).toBe(true);
            }
        })

        it('Negative test, balance is not within rule range. should throw RuleExaminationException', async () => {
            const createAccountDto: CreateAccountDto = {
                balance: 150000,
                branchCode: '001',
                customerId: 1
            }

            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 100000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            jest.spyOn(validationRule, 'validate').mockImplementationOnce(async () => {
                throw new MinimumBalanceException('Balance insufficient. Balance is less than minimum balance.')
            })
            jest.spyOn(repository, 'create').mockReturnValueOnce(testAccount);

            try {
                expect(await accountService.createNewAccount(createAccountDto)).toBeCalled();
            } catch(error) {
                expect(error instanceof MinimumBalanceException).toBe(true);
            }
        })
    })

    describe('findByAccountNo method', () => {
        it('Positive test. should return one account data', async () => {
            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }

            jest.spyOn(repository, 'findOne').mockResolvedValueOnce(testAccount);
            expect(await accountService.findAccountByAccountNo('0100000000001')).toBe(testAccount);
        })

        it('Negative test, account no not found. Should catch AccountNotFoundException', async () => {
            jest.spyOn(repository, 'findOne').mockResolvedValueOnce(null);

            try {
                expect(await accountService.findAccountByAccountNo('0100000000002')).toBeCalled();
            } catch(error) {
                expect(error instanceof AccountNotFoundException).toBe(true);
            }
        })
    })
})
import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import { RuleContext } from './../rule-context.impl';
import { Account, StatusEnum } from './../../entities/account.entity';
import { CreateAccountDto } from './../../dto/create-account.dto';
import { AccountRepository } from './../../repositories/account.repository';
import { AccountService } from './../../interfaces/services/account-service.interface';
import { AccountNoGenerator } from './../../interfaces/account-no-generator.interface';
import { AccountNotFoundException } from './../../exceptions/account-not-found.exception';

export class AccountServiceImpl implements AccountService {
    constructor(
        private readonly repository: AccountRepository,
        private readonly configService: ConfigService,
        private readonly accountNoGenerator: AccountNoGenerator,
        private readonly newAccountRule: RuleContext,
        private readonly blockRule: RuleContext,
        private readonly unblockRule: RuleContext,
        private readonly closeRule: RuleContext
    ) {}

    async createNewAccount(createAccountDto: CreateAccountDto, accountNo?: string): Promise<Account> {
        await this.newAccountRule.validate(createAccountDto);
        return await this.createAccount(createAccountDto, accountNo);
    }    
    
    async createNewEmptyAccount(createAccountDto: CreateAccountDto, accountNo: string): Promise<Account> {
        return await this.createAccount(createAccountDto, accountNo);
    }

    async blockAccount(accountNo: string): Promise<Account> {
        const account = await this.findAccountByAccountNo(accountNo);
        await this.blockRule.validate(account);
        return await this.updateAccountStatus(account, StatusEnum.Blocked);
    }

    async unblockAccount(accountNo: string): Promise<Account> {
        const account = await this.findAccountByAccountNo(accountNo);
        await this.unblockRule.validate(account);
        return await this.updateAccountStatus(account, StatusEnum.Live);
    }

    async closeAccount(accountNo: string): Promise<Account> {
        const account = await this.findAccountByAccountNo(accountNo);
        await this.closeRule.validate(account);
        return await this.updateAccountStatus(account, StatusEnum.Closed);
    }

    async findAccountByAccountNo(accountNo: string): Promise<Account> {
        const account = await this.repository.findOne({ accountNo: accountNo });
        if (!account)
            throw new AccountNotFoundException('Account not found');

        return account;
    }

    async findAccountByCustomerId(customerId: number): Promise<Account[]> {
        const accounts = await this.repository.find({ customerId: customerId });
        if (!accounts)
            return [];

        return accounts;
    }

    private async createAccount(createAccountDto: CreateAccountDto, accountNo?: string): Promise<Account> {
        const branchData = await this.getBranchData(createAccountDto.branchCode);
        const accountObj = this.repository.create(createAccountDto);

        accountObj.branchId = branchData.id;
        accountObj.openedAt = branchData.name;
        if (accountNo)
            accountObj.accountNo = accountNo;
        else
            accountObj.accountNo = await this.accountNoGenerator.generateNewNumber(createAccountDto.branchCode);

        return await this.repository.save(accountObj);
    }

    private async getBranchData(branchCode: string): Promise<any> {
        const branchUrl = this.configService.get<string>('branch.serviceUrl');
        const { data } = await axios.get(`${branchUrl}/api/branch/${branchCode}/code`);
        return data;
    }

    private async updateAccountStatus(account: Account, status: StatusEnum): Promise<Account>{
        account.status = status;
        return await this.repository.save(account);
    }
}
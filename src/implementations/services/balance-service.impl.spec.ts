import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Account, StatusEnum } from './../../entities/account.entity';
import { BalanceService } from './../../interfaces/services/balance-service.interface';
import { BalanceServiceImpl } from './../../implementations/services/balance-service.impl';
import { AccountService } from './../../interfaces/services/account-service.interface';
import { AccountServiceImpl } from './../../implementations/services/account-service.impl';
import { RuleContext } from './../rule-context.impl';
import { AccountRepository } from './../../repositories/account.repository';
import { AccountNotFoundException } from './../../exceptions/account-not-found.exception';
import { NegativeBalanceException } from './../../exceptions/negative-balance.exception';
jest.mock('./../../implementations/services/account-service.impl')
jest.mock('./../rule-context.impl')


describe('BalanceServiceImpl class', () => {
    let accountRepository: AccountRepository;
    let balanceService: BalanceService;
    let accountService: AccountService;
    let ruleValidation: RuleContext;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            providers: [
                {
                    provide: 'BalanceService',
                    useFactory: (accountRepository, accountService, validationRule) => {
                        return new BalanceServiceImpl(accountRepository, accountService, validationRule);
                    },
                    inject: [getRepositoryToken(Account), 'AccountService', 'ValidationRule']
                },
                {
                    provide: getRepositoryToken(Account),
                    useClass: AccountRepository
                },
                {
                    provide: 'AccountService',
                    useClass: AccountServiceImpl
                },
                {
                    provide: 'ValidationRule',
                    useClass: RuleContext
                }
            ]
        }).compile()

        accountRepository = moduleRef.get<AccountRepository>(getRepositoryToken(Account));
        balanceService = moduleRef.get<BalanceService>('BalanceService');
        accountService = moduleRef.get<AccountService>('AccountService');
        ruleValidation = moduleRef.get<RuleContext>('ValidationRule');
    })

    describe('substractAccountBalance method', () => {
        it('Negative test, account not found. Should throw AccountNotFoundException', async () => {
            jest.spyOn(accountService, 'findAccountByAccountNo').mockImplementationOnce(() => {
                throw new AccountNotFoundException('Account not found. Account No: 0100000000002');
            });
    
            try {
                expect(await balanceService.substractAccountBalance('0100000000002', 3000)).toBeCalled();
            } catch (error) {
                expect(error instanceof AccountNotFoundException).toBe(true);
            }
        })
        it('Positive test substraction. should return one valid account data', async () => {
            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }
    
            const testAccountAfter: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 250000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }
    
            jest.spyOn(accountService, 'findAccountByAccountNo').mockResolvedValueOnce(testAccount);
            jest.spyOn(accountRepository, 'save').mockResolvedValueOnce(testAccountAfter);
            expect(await balanceService.substractAccountBalance('0100000000001', 250000)).toStrictEqual(testAccountAfter);
        })
        it('Negative test substraction, balance insufficient. Should throw NegativeBalanceException,', async () => {
            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }
    
            jest.spyOn(ruleValidation, 'validate').mockImplementationOnce(async () => {
                throw new NegativeBalanceException('Balance insufficient. Balance is negative.');
            });
            jest.spyOn(accountService, 'findAccountByAccountNo').mockResolvedValueOnce(testAccount);
            try {
                expect(await balanceService.substractAccountBalance('0100000000001', 1000000)).toBeCalled();
            } catch (error) {
                expect(error instanceof NegativeBalanceException).toBe(true);
            }
        })
        it('Positive test addition. should return one valid account data', async () => {
            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }
    
            const testAccountAfter: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 750000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }
    
            jest.spyOn(accountService, 'findAccountByAccountNo').mockResolvedValueOnce(testAccount);
            jest.spyOn(accountRepository, 'save').mockResolvedValueOnce(testAccountAfter);
            expect(await balanceService.addAccountBalance('0100000000001', 250000)).toStrictEqual(testAccountAfter);
        })
        it('Positive test updating. should return one valid account data', async () => {
            const testAccount: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 500000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }
    
            const testAccountAfter: Account = {
                id: 1,
                accountNo: '0100000000001',
                openedAt: 'KCP Thamrin',
                balance: 300000,
                status: StatusEnum.Live,
                branchId: 1,
                customerId: 1
            }
    
            jest.spyOn(accountService, 'findAccountByAccountNo').mockResolvedValueOnce(testAccount);
            jest.spyOn(accountRepository, 'save').mockResolvedValueOnce(testAccountAfter);
            expect(await balanceService.updateAccountBalance('0100000000001', 300000)).toStrictEqual(testAccountAfter);
        })
    })
})
import axios from 'axios';
import { Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Account } from './../../entities/account.entity';
import { RuleContext } from './../rule-context.impl';
import { CheckBalanceDto } from './../../dto/check-balance.dto';
import { BalanceService } from './../../interfaces/services/balance-service.interface';
import { AccountRepository } from './../../repositories/account.repository';
import { AccountService } from './../../interfaces/services/account-service.interface';

export class BalanceServiceImpl implements BalanceService {
    constructor(
        @Inject('AccountRepository') private readonly accountRepository: AccountRepository,
        @Inject('AccountService') private readonly accountService: AccountService,
        @Inject('BalanceRule') private readonly balanceRule: RuleContext,
        private readonly configService: ConfigService
    ) {}

    async addAccountBalance(accountNo: string, amount: number): Promise<Account> {
        return await this.adjustAccountBalance(accountNo, amount, '+');
    }
    
    async substractAccountBalance(accountNo: string, amount: number): Promise<Account> {
        return await this.adjustAccountBalance(accountNo, amount, '-');
    }

    async updateAccountBalance(accountNo: string, amount: number): Promise<Account> {
        return await this.adjustAccountBalance(accountNo, amount, '=');
    }

    async substractBranchAccountBalance(branchCode: string, amount: number): Promise<Account> {
        const branchAccount = await this.getBranchAccount(branchCode);
        return await this.adjustAccountBalance(branchAccount.accountNo, amount, '-');
    }

    async addBranchAccountBalance(branchCode: string, amount: number): Promise<Account> {
        const branchAccount = await this.getBranchAccount(branchCode);
        return await this.adjustAccountBalance(branchAccount.accountNo, amount, '+');
    }

    async checkBalanceByAccountNo(accountNo: string): Promise<CheckBalanceDto> {
        const account = await this.accountService.findAccountByAccountNo(accountNo);
        const balanceDto: CheckBalanceDto = {
            accountNo: account.accountNo,
            balance: account.balance
        }
        return balanceDto;
    }

    async checkBalanceByCustomerId(customerId: number): Promise<CheckBalanceDto[]> {
        const result: CheckBalanceDto[] = [];
        const accounts = await this.accountService.findAccountByCustomerId(customerId);

        for (const account of accounts) {
            result.push({
                accountNo: account.accountNo,
                balance: account.balance
            })
        }
        return result;
    }
    
    private async adjustAccountBalance(accountNo: string, amount: number, operand: string): Promise<Account> {
        const accountData = await this.accountService.findAccountByAccountNo(accountNo);
        const operandMapper = {
            '-': this.substractBalanceAmount,
            '+': this.addBalanceAmount,
            '=': this.updateBalanceAmount
        };

        accountData.balance = operandMapper[operand](amount, Number(accountData.balance));
        await this.balanceRule.validate(accountData);
        await this.accountRepository.save(accountData);
        return accountData;
    }

    private substractBalanceAmount(amount: number, balance: number) {
        return balance - amount;
    }

    private addBalanceAmount(amount: number, balance: number) {
        return balance + amount;
    }

    private updateBalanceAmount(amount: number) {
        return amount;
    }

    private async getBranchAccount(branchCode: string): Promise<Account> {
        const customerServiceUrl = this.configService.get<string>('service.customerUrl');
        const { data } = await axios.get(`${customerServiceUrl}/api/customer/${branchCode}`);
        return await this.accountRepository.findOne({ customerId: data.id });
    }

}
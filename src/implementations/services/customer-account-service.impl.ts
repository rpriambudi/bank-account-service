import { Inject } from '@nestjs/common';
import { CustomerAccountService } from './../../interfaces/services/customer-account-service.interface';
import { AccountService } from './../../interfaces/services/account-service.interface';
import { Account } from './../../entities/account.entity';
import { AccountNotFoundException } from 'src/exceptions/account-not-found.exception';

export class CustomerAccountServiceImpl implements CustomerAccountService {
    constructor(
        @Inject('AccountService') private readonly accountService: AccountService
    ) {}

    async blockCustomerAccounts(customerId: number): Promise<Account[]> {
        const accounts = await this.accountService.findAccountByCustomerId(customerId);
        const blockPromises = accounts.map(async (account: Account): Promise<Account> => {
            try {
                return await this.accountService.blockAccount(account.accountNo);
            } catch(error) {
                return account;
            }
        });
        return Promise.all(blockPromises);
    }    
    
    async blockCustomerSelectedAccount(customerId: number, accountNo: string): Promise<Account> {
        const accounts = await this.accountService.findAccountByCustomerId(customerId);
        this.checkCustomerAccount(accountNo, accounts);

        return await this.accountService.blockAccount(accountNo);
    }
    
    async unblockCustomerAccounts(customerId: number): Promise<Account[]> {
        const accounts = await this.accountService.findAccountByCustomerId(customerId);
        const unblockPromises = accounts.map(async (account: Account): Promise<Account> => {
            try {
                return await this.accountService.unblockAccount(account.accountNo);
            } catch(error) {
                return account;
            }
        });
        return Promise.all(unblockPromises);
    }
    
    async unblockCustomerSelectedAccount(customerId: number, accountNo: string): Promise<Account> {
        const accounts = await this.accountService.findAccountByCustomerId(customerId);
        this.checkCustomerAccount(accountNo, accounts);

        return await this.accountService.unblockAccount(accountNo);
    }

    async closeCustomerAccounts(customerId: number): Promise<Account[]> {
        const accounts = await this.accountService.findAccountByCustomerId(customerId);
        const closePromises = accounts.map(async (account: Account): Promise<Account> => {
            try {
                return await this.accountService.closeAccount(account.accountNo);
            } catch(error) {
                return account;
            }
        });
        return Promise.all(closePromises);
    }

    async closeCustomerSelectedAccount(customerId: number, accountNo: string): Promise<Account> {
        const accounts = await this.accountService.findAccountByCustomerId(customerId);
        this.checkCustomerAccount(accountNo, accounts);

        return await this.accountService.closeAccount(accountNo);
    }

    private checkCustomerAccount(accountNo: string, accounts: Account[]): void {
        const customerAccount = accounts.reduce((prev: Account, current: Account) => {
            if (current.accountNo === accountNo)
                return current;
        }, null);

        if (!customerAccount)
            throw new AccountNotFoundException('selected customer account not found');
    }
}
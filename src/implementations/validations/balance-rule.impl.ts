import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Account } from './../../entities/account.entity';
import { BusinessRule } from './../../interfaces/business-rule.interface';
import { NegativeBalanceException } from './../../exceptions/negative-balance.exception';
import { MinimumBalanceException } from './../../exceptions/minimum-balance.exception';

@Injectable()
export class PositiveBalanceRule implements BusinessRule {
    validate(validationData: Account): Promise<void> {
        return new Promise((resolve, reject) => {
            if (validationData.balance < 0)
                reject(new NegativeBalanceException('Balance amount is insufficient.'));
            
            resolve();
        })
    }
}

@Injectable()
export class MinimumBalanceRule implements BusinessRule {
    constructor(
        private readonly configService: ConfigService
    ) {}

    validate(validationData: Account): Promise<void> {
        return new Promise((resolve, reject) => {
            const minimumBalance = this.configService.get<number>('account.minimum');
            if (!minimumBalance)
                resolve();

            if (validationData.balance < minimumBalance)
                reject(new MinimumBalanceException('Balance amount is insufficient.'));

            resolve();
        })
    }
}
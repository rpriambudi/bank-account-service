import { BusinessRule } from './../../interfaces/business-rule.interface';
import { Account, StatusEnum } from './../../entities/account.entity';
import { ClosedAccountException } from './../../exceptions/closed-account.exceptions';
import { BlockedAccountException } from './../../exceptions/blocked-account.exception';

export class ClosedStatusRule implements BusinessRule {
    validate(validationData: Account): Promise<void> {
        return new Promise((resolve, reject) => {
            if (validationData.status === StatusEnum.Closed)
                reject(new ClosedAccountException('Operation is not permitted. Account is closed.'));

            if (validationData.status === StatusEnum.Blocked)
                reject(new BlockedAccountException('Operation is not permitted. Account is already blocked.'));
            resolve();
        });
    }
}

export class BlockedStatusRule implements BusinessRule {
    validate(validationData: Account): Promise<void> {
        return new Promise((resolve, reject) => {
            if (validationData.status === StatusEnum.Blocked)
                reject(new BlockedAccountException('Operation is not permitted. Account is blocked.'));

            if (validationData.status === StatusEnum.Closed)
                reject(new ClosedAccountException('Operation is not permitted. Account already closed'));
            resolve();
        });
    }
}

export class UnblockStatusRule implements BusinessRule {
    validate(validationData: Account): Promise<void> {
        return new Promise((resolve, reject) => {
            if (validationData.status === StatusEnum.Live)
                reject(new BlockedAccountException('Operation is not permitted. Account is not blocked.'));

            if (validationData.status === StatusEnum.Closed)
                reject(new ClosedAccountException('Operation is not permitted. Account is already closed.'));
            resolve();
        });
    }
}
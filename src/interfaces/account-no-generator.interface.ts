export interface AccountNoGenerator {
    generateNewNumber(branchCode: string): Promise<string>;
}
export interface BusinessRule {
    validate(validationData: any): Promise<void>;
}
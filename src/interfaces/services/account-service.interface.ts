import { Account } from './../../entities/account.entity';
import { CreateAccountDto } from './../../dto/create-account.dto';

export interface AccountService {
    createNewAccount(createAccountDto: CreateAccountDto, accountNo?: string): Promise<Account>;
    createNewEmptyAccount(createAccountDto: CreateAccountDto, accountNo: string): Promise<Account>;
    blockAccount(accountNo: string): Promise<Account>;
    unblockAccount(accountNo: string): Promise<Account>;
    closeAccount(accountNo: string): Promise<Account>;
    findAccountByAccountNo(accountNo: string): Promise<Account>;
    findAccountByCustomerId(customerId: number): Promise<Account[]>;
}
import { Account } from './../../entities/account.entity';
import { CheckBalanceDto } from './../../dto/check-balance.dto';

export interface BalanceService {
    addAccountBalance(accountNo: string, amount: number): Promise<Account>;
    substractAccountBalance(accountNo: string, amount: number): Promise<Account>;
    updateAccountBalance(accountNo: string, amount: number): Promise<Account>;
    addBranchAccountBalance(branchCode: string, amount: number): Promise<Account>;
    substractBranchAccountBalance(branchCode: string, amount: number): Promise<Account>;
    checkBalanceByAccountNo(accountNo: string): Promise<CheckBalanceDto>;
    checkBalanceByCustomerId(customerId: number): Promise<CheckBalanceDto[]>;
}
import { Account } from './../../entities/account.entity';

export interface CustomerAccountService {
    blockCustomerAccounts(customerId: number): Promise<Account[]>;
    blockCustomerSelectedAccount(customerId: number, accountNo: string): Promise<Account>;
    unblockCustomerAccounts(customerId: number): Promise<Account[]>;
    unblockCustomerSelectedAccount(customerId: number, accountNo: string): Promise<Account>;
    closeCustomerAccounts(customerId: number): Promise<Account[]>;
    closeCustomerSelectedAccount(customerId: number, accountNo: string): Promise<Account>;
}
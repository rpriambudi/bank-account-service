import { IBrokerConsumer, HttpGenericFilter } from 'bank-shared-lib';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import exceptionCode from './../exception-code.json';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const consumer = app.select(AppModule).get<IBrokerConsumer>('BrokerConsumer');
  consumer.consume();
  app.useGlobalFilters(new HttpGenericFilter(exceptionCode));
  await app.listen(3004);
}
bootstrap();

import { ConfigService } from '@nestjs/config';
import { AuthJwtMiddleware } from 'bank-shared-lib';
import { Inject } from '@nestjs/common';
export class BankAuthMiddleware extends AuthJwtMiddleware {
    constructor(
        @Inject('ConfigService') private readonly configService: ConfigService
    ) {super();}
    
    getJwksUrl(): string {
        return this.configService.get<string>('auth.jwksUrl');
    }

    getClientId(): string {
        return this.configService.get<string>('auth.clientId');
    }   
    
    getGuards(): string[] {
        return ['Teller'];
    }
    
}
import {MigrationInterface, QueryRunner} from "typeorm";

export class SequenceMigration1587886295991 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE SEQUENCE IF NOT EXISTS  account_no_sequence');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DROP SEQUENCE IF EXISTS account_no_sequence');
    }

}
